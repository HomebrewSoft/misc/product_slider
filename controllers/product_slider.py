# -*- coding: utf-8 -*-

from odoo import http


class ProductSliderController(http.Controller):

    @http.route(['/shop/get_product_slider_content'], type='http', auth='public', website=True)
    def get_product_slider_content(self, **post):
        product_ids = http.request.env['product.product'].search([])
        return http.request.render("product_slider.product_slider_content", {'products': product_ids})
