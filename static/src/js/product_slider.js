odoo.define('product_slider.product_slider', function (require) {
    'use strict';
    var animation = require('website.content.snippets.animation');

    animation.registry.product_slider = animation.Class.extend({
        selector: ".product_slider",
        start: function () {
            var self = this;
            if (!this.editableMode) {
                $.get("/shop/get_product_slider_content", {}).then(function (data) {
                    if (data) {
                        self.$target.empty().append(data);
                    }
                });
            }
        }
    });
});
