# -*- coding: utf-8 -*-
{
    'name': 'Product Slider',
    'version': '11.0.0.1.0',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
        'website',
    ],
    'data': [
        # security
        # data
        # templates
        'templates/product_slider.xml',
        # reports
        # views
    ],
}
